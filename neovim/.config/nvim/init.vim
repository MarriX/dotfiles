call plug#begin('~/.local/share/nvim/plugged')

" Airline {{{
 Plug 'vim-airline/vim-airline'
 Plug 'vim-airline/vim-airline-themes'
 let g:airline_powerline_fonts = 1
 let g:airline_theme='base16'
" }}}

" Neomake
Plug 'neomake/neomake'

" colorscheme {{{
Plug 'duythinht/vim-coffee'
" colorscheme coffee
" }}}

" Ranger {{{
Plug 'rbgrouleff/bclose.vim' | Plug 'francoiscabrol/ranger.vim'
" }}}

" Editing S-expression with the power of vim
Plug 'guns/vim-sexp'

" Lexima (mi vida) {{{
Plug 'cohama/lexima.vim'
" }}}
Plug 'easymotion/vim-easymotion'

call plug#end()

" Neomake {{{
" When writing a buffer.
call neomake#configure#automake('w')
" When writing a buffer, and on normal mode changes (after 750ms).
call neomake#configure#automake('nw', 750)
" When reading a buffer (after 1s), and when writing.
call neomake#configure#automake('rw', 1000)

"}}}

set termguicolors
colorscheme coffee
set number
set relativenumber

" Use persistent history.
set undodir=~/.config/nvim/undodir/
set undofile

" op completion
filetype plugin on
set omnifunc=syntaxcomplete#Complete
set path+=**
